# Grover Generator Model
Grover is a model for Neural Fake News -- both generation and detection. However, it probably can also be used for other generation tasks. Here we are considering Grover only for Generating task.
Reference:
Code for Defending Against Neural Fake News
Project page at rowanzellers.com/grover, the AI2 online demo, or read the full paper at arxiv.org/abs/1905.12616.

## What's in this repo?

* Code for the Grover generator (in lm/). This involves training the model as a language model across fields.
* Code for generating from a Grover model, in sample/.
* Code for making your own RealNews dataset in realnews/.
Model checkpoints freely available online for all of the Grover models. For using the RealNews dataset for research, please submit this form.

Scroll down 👇 for some easy-to-use instructions for setting up Grover to generate news articles.
NOTE: If you just care about making your own RealNews dataset, you will need to set up your environment separately just for that, using an AWS machine (see realnews/.)

Quickstart: setting up Grover for generation!

Set up your environment. Here's the easy way, assuming anaconda is installed: conda create -y -n grover python=3.6 && source activate grover && pip install -r requirements-gpu.txt

Download the model using python download_model.py base

Now generate: python sample/contextual_generate.py -model_config_fn lm/configs/base.json -model_ckpt models/base/model.ckpt -metadata_fn sample/april2019_set_mini.jsonl -out_fn april2019_set_mini_out.jsonl


Congrats! You can view the generations, conditioned on the domain/headline/date/authors, in april2019_set_mini_out.jsonl.

# Text Similarity Using Siamese LSTM Deep Neural Network
Siamese neural network is a class of neural network architectures that contain two or more identical subnetworks. identical here means they have the same configuration with the same parameters and weights. Parameter updating is mirrored across both subnetworks.
It is a keras based implementation of deep siamese Bidirectional LSTM network to capture phrase/sentence similarity using word embeddings.

## What's in this repo?

* Code containing the configuration details such as dropout rate, number of LSTM layers, activation function etc., in config.py
* Code containing the embedding technique in controller.py
* Code containing the preprocessing of input data in inputHandler.py
* Code containing the model details in model.py

Datasets are present in \Dataset

Install dependencies
pip install -r requirements.txt
References:
Siamese Recurrent Architectures for Learning Sentence Similarity (2016).
Inspired from Tensorflow Implementation of https://github.com/amansrivastava17/lstm-siamese-text-similarity and https://github.com/dhwajraj/deep-siamese-text-similarity.